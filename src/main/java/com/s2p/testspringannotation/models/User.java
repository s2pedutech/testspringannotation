/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2p.testspringannotation.models;

/**
 *
 * @author hp
 */
public class User {
    private int id;
    private String email;
    private String password;
    private String mob;

    public User() {
    }

    public User(int id, String email, String password, String mob) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.mob = mob;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", email=" + email + ", password=" + password + ", mob=" + mob + '}';
    }
    
    
}
