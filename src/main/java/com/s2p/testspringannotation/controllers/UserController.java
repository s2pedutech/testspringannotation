/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2p.testspringannotation.controllers;

import com.s2p.testspringannotation.dao.UserDAO;
import com.s2p.testspringannotation.models.User;
import com.s2p.testspringannotation.services.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author hp
 */
@RestController
@RequestMapping("/users1")
//@ResponseBody
public class UserController {
    
    @Autowired
    public UserService UserService;
//    
//    @Autowired
//    public UserDAO userDAO;
    @RequestMapping(method=RequestMethod.GET)
    public List<User> getAll()
    {
        User u1 = new User();
        User u2 = new User();
        ArrayList<User> al = new ArrayList<>();
        al.add(u1);
        al.add(u2);
        return UserService.getAll();
//        return al;
    }
}
