/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2p.testspringannotation.services;

import com.s2p.testspringannotation.dao.UserDAO;
import com.s2p.testspringannotation.models.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author hp
 */
@Service("UserService")
public class UserServiceImpl implements UserService{

    @Autowired
    public UserDAO userDAO;
    
    @Override
    public List<User> getAll() {
        return userDAO.getUsers();
    }
    
}
