/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2p.testspringannotation.dao;

import com.s2p.testspringannotation.models.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hp
 */
@Repository("userDAO")
public class UserDAOImpl implements UserDAO { 
    @Autowired
    JdbcTemplate jdbcTemplate;
    public List<User> getUsers(){
        List<User> l = jdbcTemplate.query("select * from user", new UserRowMapper());
        return l;
    }
}

class UserRowMapper implements RowMapper<User>{

    @Override
    public User mapRow(ResultSet rs, int i) throws SQLException {
        User u = new User();
        u.setId(rs.getInt("id"));
        u.setEmail(rs.getString("email"));
        u.setPassword(rs.getString("password"));
        u.setMob(rs.getString("mob"));
        return u;
    }
    
}