/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2p.testspringannotation.dao;

import com.s2p.testspringannotation.models.User;
import java.util.List;

/**
 *
 * @author hp
 */
public interface UserDAO {
    public List<User> getUsers();
}
